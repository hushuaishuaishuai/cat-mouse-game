package dao

import (
	m "cat_mouse/core/model"
	"cat_mouse/infra/dao"
	"cat_mouse/infra/model"
)

type UserDao struct {
	dao.BaseDao
}

func NewUserDao() *UserDao {
	return &UserDao{}
}

func (u *UserDao) GetByIds(ids []string) ([]*m.User, error) {
	var (
		rows = make([]*m.User, 0)
		err  error
	)
	err = model.DB.Model(&m.User{}).Where("user_id in (?)", ids).Scan(&rows).Error
	return rows, err
}
