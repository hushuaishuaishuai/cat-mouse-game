package dao

import (
	m "cat_mouse/core/model"
	"cat_mouse/infra/dao"
	"cat_mouse/infra/model"
	"time"
)

type TokenDao struct {
	dao.BaseDao
}

func NewTokenDao() *TokenDao {
	return &TokenDao{}
}

func (t *TokenDao) DelExpiredToken() error {
	return model.DB.Where("expire_at < ?", time.Now()).Delete(&m.Token{}).Error
}
