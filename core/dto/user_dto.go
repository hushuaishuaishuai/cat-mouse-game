package dto

import m "cat_mouse/core/model"

type UserLoginReq struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type UserUpdateReq struct {
	Username        string `json:"username" binding:"omitempty,min=1,max=20"`
	Sex             *int   `json:"sex" binding:"omitempty,oneof=0 1 2"`
	Avatar          string `json:"avatar" binding:"omitempty"`
	BackgroundCover string `json:"backgroundCover" binding:"omitempty"`
	Birthday        string `json:"birthday" binding:"omitempty"`
	Phone           string `json:"phone" binding:"omitempty"`
	Constellation   string `json:"constellation" binding:"omitempty"`
	Email           string `json:"email" binding:"omitempty"`
	RegistryPlace   string `json:"registryPlace" binding:"omitempty"`
	Hometown        string `json:"hometown" binding:"omitempty"`
	Label           string `json:"label" binding:"omitempty"`
	Career          string `json:"career" binding:"omitempty"`
	Signature       string `json:"signature" binding:"omitempty"`
	Status          *int   `json:"status" binding:"omitempty,oneof=0 1"`
}

type UserAlbums struct {
	AlbumSrc []string `json:"albumSrc" binding:"min=1,max=5"`
}

type UserDetail struct {
	*m.UserInfo
}

type UserQueryDto struct {
	PageQuery
	UserId   string `form:"userId" binding:"omitempty"`
	Username string `form:"username" binding:"omitempty"`
}
