package dto

type ValidCodeReq struct {
	Phone string `json:"phone" binding:"required"`
}
