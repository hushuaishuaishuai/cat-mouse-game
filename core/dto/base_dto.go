package dto

type PageQuery struct {
	Page  int `form:"page" binding:"omitempty,gt=0"`
	Limit int `form:"limit"`
}
