package core

import (
	"cat_mouse/core/cron"
	"cat_mouse/core/settings"
	"cat_mouse/infra/model"
	"cat_mouse/infra/utils/log"
	"cat_mouse/infra/utils/redis"
	"github.com/gin-gonic/gin"
)

func setUp() {
	settings.Setup()
	redis.SetUp(settings.Config.Redis)
	log.Setup()
	model.Setup()
	cron.Timer()
}

func Start() {
	setUp()
	// WebSocket管理服务启动
	//go infraSvc.GetWsClientManager().Start()
	gin.SetMode(settings.Config.Server.RunMode)
	routers := InitRouter()
	if err := routers.Run("0.0.0.0:" + settings.Config.Server.HttpPort); err != nil {
		log.Logger.Fatalf("服务启动失败: %v", err)
	}
}
