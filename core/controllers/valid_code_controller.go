package controllers

import (
	"cat_mouse/core/dto"
	"cat_mouse/core/service"
	"cat_mouse/infra/common"
	"cat_mouse/infra/response"
	"cat_mouse/infra/utils/log"
	"github.com/gin-gonic/gin"
)

type ValidCodeController struct {
	validCodeService service.IValidCodeService
}

func ValidCodeControllerRegister(router *gin.RouterGroup) {
	validCodeController := ValidCodeController{
		service.NewValidCodeService(),
	}
	router.POST("", validCodeController.GetValidCode)
}

func (v *ValidCodeController) GetValidCode(c *gin.Context) {
	var (
		code   string
		reqDto = dto.ValidCodeReq{}
		err    error
	)
	if err = c.BindJSON(&reqDto); err != nil {
		response.InputError(c, err.Error())
		return
	}
	if code, err = v.validCodeService.SendValidCode(reqDto.Phone); err != nil {
		log.Logger.Errorf("%+v", err)
		response.InternalError(c, common.SystemError)
		return
	}
	response.OK(c, code)
}
