package service

import (
	"cat_mouse/core/dao"
	"cat_mouse/core/dto"
	m "cat_mouse/core/model"
	"sync"
)

type IUserService interface {
	GetUser(id string) (*m.UserInfo, error)
	UserLogin(req *dto.UserLoginReq) (string, error)
	UpdateUserInfo(userId string, req *dto.UserUpdateReq) error
}

type UserService struct {
	userDao  *dao.UserDao
	tokenDao *dao.TokenDao
}

func (u *UserService) UserLogin(req *dto.UserLoginReq) (string, error) {
	//TODO implement me
	panic("implement me")
}

func (u *UserService) ListUser(userInfo *m.UserInfo, queryDto *dto.UserQueryDto) (rows []*m.BasicUser, total int64, err error) {
	return nil, 0, nil
}

func (u *UserService) UpdateUserInfo(userId string, req *dto.UserUpdateReq) (err error) {
	return err
}

func (u *UserService) GetUser(id string) (*m.UserInfo, error) {
	return nil, nil
}

var (
	userSvc     *UserService
	userSvcOnce sync.Once
)

func NewUserService() IUserService {
	userSvcOnce.Do(func() {
		userSvc = &UserService{
			userDao:  dao.NewUserDao(),
			tokenDao: dao.NewTokenDao(),
		}
	})
	return userSvc
}
