package cron

import (
	"cat_mouse/core/service"
	"cat_mouse/infra/common"
	"cat_mouse/infra/utils/log"
)

func Timer() {
	_, _ = common.TimerCron.AddFunc("*/1 * * * *", Ping)
	_, _ = common.TimerCron.AddFunc("0 3 * * *", ClearExpiredToken)
	common.TimerCron.Start()
}

func Ping() {
	//panic("Ping")
}

func SyncTopicCache() {
	//TODO 定时同步朋友圈缓存，2小时执行一次
}

func ClearExpiredToken() {
	log.Logger.Infof("定时任务(清理过期token)开始执行")
	service.NewTokenService().ClearToken()
	log.Logger.Infof("定时任务(清理过期token)执行完毕")
}
