package middlewares

import (
	m "cat_mouse/core/model"
	"cat_mouse/core/service"
	"cat_mouse/infra/common"
	"cat_mouse/infra/utils"
	"cat_mouse/infra/utils/log"
	"cat_mouse/infra/utils/redis"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"time"
)

func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			token      = c.Request.Header.Get("token")
			noAuthUrls = utils.GetSysCfg().NoAuthUrl
			userId     string
			user       *m.UserInfo
			err        error
		)
		if v, ok := noAuthUrls[c.Request.URL.String()]; ok && c.Request.Method == v {
			c.Next()
			return
		}
		if userId, err = validToken(token); err != nil {
			goto Abort
		}
		if user, err = service.NewUserService().GetUser(userId); err == nil {
			params := map[string]float64{
				user.UserId: float64(time.Now().Unix()),
			}
			_ = redis.ZAdd(common.UserLatestLogin, params)
			c.Set("userInfo", user)
			c.Next()
			return
		}
		log.Logger.Error(err)
	Abort:
		c.JSON(401, gin.H{
			"status": 401,
			"msg":    "用户认证失败",
			"data":   "failure",
		})
		c.Abort()
	}
}

func validToken(token string) (userId string, err error) {
	var (
		userIdStr string
	)
	if userIdStr, err = redis.Get(token); err == nil && userIdStr != "" {
		err = json.Unmarshal([]byte(userIdStr), &userId)
		// token验证成功，进行续命操作，注意的是这里没有对数据库中的token进行续命
		_ = redis.Expire(token, utils.GetSysCfg().TokenExpire)
		return
	}
	if userIdStr == "" {
		err = errors.New("token expired")
	}
	return
}
