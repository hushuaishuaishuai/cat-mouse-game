package model

import (
	"cat_mouse/infra/common"
	"cat_mouse/infra/model"
)

// ValidCode 短信验证码
type ValidCode struct {
	model.BaseModel
	Phone    string          `gorm:"not null;" json:"phone"`
	Code     string          `gorm:"not null;" json:"code"`
	ExpireAt common.JSONTime `gorm:"default:null;" json:"expireAt"`
}
