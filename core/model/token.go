package model

import (
	"cat_mouse/infra/common"
	"cat_mouse/infra/model"
)

type Token struct {
	model.BaseModel
	UserId   string          `gorm:"default:null" json:"userId"`
	Token    string          `gorm:"default:null" json:"token"`
	ExpireAt common.JSONTime `gorm:"default:null" json:"expireAt"`
}
