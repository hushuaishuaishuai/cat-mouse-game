package model

import (
	"cat_mouse/infra/model"
)

type BasicUser struct {
	UserId          string `gorm:"default:null" json:"userId"`
	Username        string `gorm:"default:null" json:"username"`
	Avatar          string `gorm:"default:null" json:"avatar"`
	BackgroundCover string `gorm:"default:null" json:"backgroundCover"`
	Sex             int    `gorm:"default:null" json:"sex"`
	Birthday        string `gorm:"default:null" json:"birthday"`
	Phone           string `gorm:"default:null" json:"phone"`
	Email           string `gorm:"default:null" json:"email"`
	Hometown        string `gorm:"default:null" json:"hometown"`
	RegistryPlace   string `gorm:"default:null" json:"registryPlace"`
	Career          string `gorm:"default:null" json:"career"`
	Constellation   string `gorm:"default:null" json:"constellation"`
	VipLevel        int    `gorm:"not null" json:"vipLevel"`
	Signature       string `gorm:"default:null" json:"signature"`
	Status          int    `gorm:"not null" json:"status"` // 0-正常 1-删除
	Admin           int    `gorm:"not null" json:"admin"`  // 0-普通用户 1-管理员
}

type User struct {
	model.BaseModel
	BasicUser
	Password string `gorm:"default:null" json:"password"`
	Salt     string `gorm:"default:null" json:"salt"`
}

type UserInfo struct {
	*BasicUser
}
