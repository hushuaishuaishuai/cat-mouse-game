package config

import "time"

type System struct {
	LogLevel string `yaml:"log-level"`
}

type Server struct {
	RunMode         string
	HttpPort        string
	LogSavePath     string
	LogMaxAge       time.Duration
	LogRotationTime time.Duration
	LogLevel        string
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	EnabledSwagger  bool
}

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Name        string
	TablePrefix string
}

type Redis struct {
	Host           string
	Password       string
	MaxIdle        int
	MaxActive      int
	IdleTimeout    time.Duration
	ConnectTimeout time.Duration
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration
}

type AuthCodeJwt struct {
	Secret string `yaml:"secret"`
	Issuer string `yaml:"issuer"`
	Expire int    `yaml:"expire"`
}

type QiNiuYun struct {
	Host          string `yaml:"host"`
	AccessKey     string `yaml:"accessKey"`
	SecretKey     string `yaml:"secretKey"`
	TopicBucket   string `yaml:"topicBucket"`
	CommentBucket string `yaml:"commentBucket"`
	ReplyBucket   string `yaml:"replyBucket"`
	DefaultExpire uint64 `yaml:"defaultExpire"`
}

type ALiYunOss struct {
	RegionId        string `yaml:"regionId"`
	Endpoint        string `yaml:"endpoint"`
	AccessKey       string `yaml:"accessKey"`
	SecretKey       string `yaml:"secretKey"`
	RoleSessionName string `yaml:"roleSessionName"`
	RoleArn         string `yaml:"roleArn"`
	AvatarBucket    string `yaml:"avatarBucket"`
	AlbumBucket     string `yaml:"albumBucket"`
}

type ALiYun struct {
	RegionId        string `yaml:"regionId"`
	AccessKeyId     string `yaml:"accessKeyId"`
	AccessKeySecret string `yaml:"accessKeySecret"`
	Scheme          string `yaml:"scheme"`
	SignName        string `yaml:"signName"`
	TemplateCode    string `yaml:"templateCode"`
	Method          string `yaml:"method"`
	Domain          string `yaml:"domain"`
	Version         string `yaml:"version"`
	ApiName         string `yaml:"apiName"`
	Message         string `yaml:"message"`
}

type OpenIm struct {
	Host        string `yaml:"host"`
	Secret      string `yaml:"secret"`
	AdminUserId string `yaml:"adminUserId"`
}
