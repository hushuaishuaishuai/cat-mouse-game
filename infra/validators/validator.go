package validators

import (
	"github.com/go-playground/validator/v10"
	"reflect"
	"regexp"
)

// NotAllowBlank 验证字符串是否包含空格
func NotAllowBlank(fl validator.FieldLevel) bool {
	verificationRole := `\s`
	field := fl.Field()
	switch field.Kind() {
	case reflect.String:
		re, err := regexp.Compile(verificationRole)
		if err != nil {
			return false
		}
		return !re.MatchString(field.String())
	default:
		return true
	}
}

// NotAllowSideBlank 验证字符串两边是否包含空格
func NotAllowSideBlank(fl validator.FieldLevel) bool {
	verificationRole := `(^\s)|(\s$)`
	field := fl.Field()
	switch field.Kind() {
	case reflect.String:
		re, err := regexp.Compile(verificationRole)
		if err != nil {
			return false
		}
		return !re.MatchString(field.String())
	default:
		return true
	}
}

// Phone 验证字符串是否是手机号码
func Phone(fl validator.FieldLevel) bool {
	field := fl.Field()
	switch field.Kind() {
	case reflect.String:
		regRuler := "^1[345789]{1}\\d{9}$"
		reg := regexp.MustCompile(regRuler)
		return reg.MatchString(field.String())
	default:
		return true
	}
}
