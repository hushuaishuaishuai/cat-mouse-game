package message

import (
	"cat_mouse/infra/utils/message"
	"strings"
)

// ShortMessage 定义短信发送接口
type ShortMessage interface {
	SendMessage(phoneNumber, code string)
}

// ShortMsgFactory 定义短信发送工厂
func ShortMsgFactory(name string) ShortMessage {
	switch strings.ToLower(name) {
	case "aliyun":
		return message.NewALiYunMsgClient()
	default:
		panic("Not Found")
	}
}
