package common

import "time"

const (
	ModuleCore = "core"
)

const (
	TimeLayout            = "2006-01-02 15:04:05"
	DateLayout            = "200601"
	LogFileNameDateLayout = "2006-01-02"
)

const (
	// WriteWait Time allowed to write a message to the peer.
	WriteWait = 10 * time.Second
	// PongWait Time allowed to read the next pong message from the peer.
	PongWait = 60 * time.Second
	// PingPeriod Send pings to peer with this period. Must be less than pongWait.
	PingPeriod = (PongWait * 9) / 10
	// MaxMessageSize Maximum message size allowed from peer.
	MaxMessageSize = 512
)

// 定义Redis Key
const (
	SysCfgRedisKey          = "system-config"
	UserFollowsCacheKey     = "user:%s:follows"
	UserFollowCacheLock     = "user:%s:follows:lock"
	UserFansCacheKey        = "user:%s:fans"
	UserFansCacheLock       = "user:%s:fans:lock"
	UserFriendsCacheKey     = "user:%s:friends"
	UserFriendsCacheLock    = "user:%s:friends:lock"
	UserTopicCacheKey       = "user:%s:topics"
	UserVisitorCacheKey     = "user:%s:visitors"
	RedisValidCodePrefix    = "message:code:%s"
	RedisUserPrefix         = "user:%s"
	RedisUserLogin          = "user:login"
	UserSignupCacheKey      = "user:%s:signup:%s"
	UserLatestLogin         = "user:latest:login"
	UserLabelCacheKey       = "user:labels"
	UserCareerCacheKey      = "user:career"
	TopicLikeCacheKey       = "topic:%d:like"
	OpenImUserTokenCacheKey = "open-im:users:%s:token"
	TopicCacheKey           = "topic:like"
)

const (
	TopicNormalStatus         = 0
	TopicDelStatus            = 1
	TopicPublicPermission     = 0
	TopicHomeOnlyPermission   = 1
	TopicPrivatePermission    = 2
	TopicFriendOnlyPermission = 3
	CommentNormalStatus       = 0
	CommentDelStatus          = 1
	ReplyNormalStatus         = 0
	ReplyDelStatus            = 1
	OneWayFollowStatus        = 0
	TwoWayFollowStatus        = 1
	AdminUser                 = 1
	NormalUser                = 0
	UserNormalStatus          = 0
	UserDelStatus             = 1
)
