package common

const (
	Success              = "ok"
	InvalidParams        = "请求参数错误"
	ParamEmptyError      = "参数[%s]不能为空"
	GoroutineError       = "Goroutine内部函数异常"
	InternalError        = "系统服务异常"
	ParseSysCfgError     = "系统配置文件解析异常"
	GetSysCfgError       = "系统配置文件读取异常"
	SystemError          = "系统异常"
	ForbiddenError       = "无权限操作"
	RedisServerError     = "Redis服务异常"
	RedisSetError        = "写入Redis异常"
	GetRedisLockError    = "获取Redis锁失败"
	GetRedisLockOK       = "获取Redis锁成功"
	SearchMySQLError     = "查询MySQL数据库异常"
	InsertMySQLError     = "插入MySQL数据库异常"
	VisitUserCenterError = "访问用户中心微服务异常"
	NotAuth              = "没有权限"
	JsonParseError       = "JSON解析异常"
	ParseRespBodyError   = "解析Body异常"
	UserNotExist         = "用户不存在"
)

const (
	RepeatFollowMsg           = "用户重复关注"
	NameOrPasswordCanNotEmpty = "用户名密码不能为空"
	UserNotFound              = "用户未注册"
	PhoneOrCodeCanNotEmpty    = "手机号码或验证码不能为空"
	PhoneCodeError            = "验证码校验失败"
	PhoneCodeExpired          = "验证码已过期"
	WeChatCodeCanNotEmpty     = "微信code不能为空"
	WeChatGetAccessTokenError = "微信获取access_token失败"
	WeChatGetUserDetailError  = "微信获取用户详细信息失败"
	WeChatGetCodeError        = "微信获取授权code失败"
	PasswordLoginError        = "用户名或密码错误"
	GetOrCreateUserError      = "获取或创建用户失败"
	SmsNetworkError           = "短信网关网络异常"
	SmsFrequencyTooFast       = "短信发送频率过快"
)
