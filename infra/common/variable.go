package common

import (
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/websocket"
	"github.com/robfig/cron/v3"
	"sync"
)

var (
	// TimerCron 定义全局定时器
	TimerCron = cron.New()
	// ValidTrans 定义错误翻译对象
	ValidTrans  ut.Translator
	ValidObj    *validator.Validate
	RoomEngines sync.Map
)

var WsUpGrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type HandlerCountFunc func(userId string) (int64, error)

type LoadCacheHandler func(userId string) error
