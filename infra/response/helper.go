package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

const (
	// SystemCode 系统编码
	SystemCode = "22"
	// SystemIgnoreLevel 系统可忽略级别
	SystemIgnoreLevel = "2"
	// SystemWarnLevel 系统警告级别
	SystemWarnLevel = "3"
	// SystemErrorLevel 系统报错级别
	SystemErrorLevel = "4"
)

func OK(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"status": "200",
		"msg":    "success",
		"data":   data,
	})
}

func PageOK(c *gin.Context, data interface{}, total interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"status": "200",
		"msg":    "success",
		"data":   data,
		"total":  total,
	})
}

func releaseCode(status string, level string) string {
	return level + "-" + SystemCode + "-" + status
}

func InputError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("400", "4"),
		"msg":    msg,
	})
}

func AuthError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("401", "4"),
		"msg":    msg,
	})
}

func ForbiddenError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("403", "4"),
		"msg":    msg,
	})
}

func NotFoundError(c *gin.Context, msg []string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("404", "4"),
		"msg":    msg,
	})
}

func InternalError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("500", "4"),
		"msg":    msg,
	})
}

func GatewayError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("502", "4"),
		"msg":    msg,
	})
}

func FuncError(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": releaseCode("501", "4"),
		"msg":    msg,
	})
}

func Forbidden(c *gin.Context, msg string) {
	c.JSON(http.StatusOK, gin.H{
		"status": 403,
		"msg":    msg,
	})
}
